#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <openssl/rand.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include "pass_store.h"

#define SALT_LEN 8

// Every 3 bytes of salt encoded as 4 characters + possible padding
#define SALT_LEN_BASE64 (SALT_LEN/3 + 1) * 4
#define SHA512_DIGEST_LENGTH_BASE64 (SHA512_DIGEST_LENGTH/3 + 1) * 4

#define PASSWORD_HASH_TYPE 6  // SHA-512

#define MAX_USER_LEN 32
#define PASS_FILE_PATH "passwords"

typedef struct user_pass_s {
  // NULL-terminated username string
  // if username is empty, consider the entry removed
  char username[MAX_USER_LEN];
  // binary password hash (no encoding)
  uint8_t pass_hash[SHA512_DIGEST_LENGTH];
  // NULL-terminated Base64 encoded salt string
  char salt[SALT_LEN_BASE64+1];
} user_pass_t;


static int __pass_store_load(user_pass_t **passwords_out, size_t *num_pass_out)
{
  FILE *passFile;
  char *currentLine = NULL;
  size_t placeHolder = 0;
  *num_pass_out = 0;

  int columnNum, 
      usernameNum, 
      saltNum, 
      hashNum;
  
  uint8_t pw_buf[SHA512_DIGEST_LENGTH_BASE64];

  passFile = fopen(PASS_FILE_PATH, "r");
  if (passFile == NULL){
    return -1;
  }
  
  while (getline(&currentLine, &placeHolder, passFile) != -1){
    columnNum = 0;
    usernameNum = 0;
    saltNum = 0;
    hashNum = 0;

    // Write username into password struct username array
    while (currentLine[columnNum] != ':'){
      passwords_out[*num_pass_out]->username[usernameNum] = currentLine[columnNum];
      usernameNum++;
      columnNum++;
    }

    // Write the null character to the end of the password struct username
    passwords_out[*num_pass_out]->username[usernameNum] = '\0';
    
    columnNum += 4;

    //GET SALT

    while (currentLine[columnNum] != '$'){
      passwords_out[*num_pass_out]->salt[saltNum] = currentLine[columnNum];
      saltNum++;
      columnNum++;
    }
    
    // Write the null character to the end of the password struct salt
    passwords_out[*num_pass_out]->salt[saltNum] = '\0';

    columnNum++; //skip


    // Transfer encoded password hash into the pw_buf buffer
    while(currentLine[columnNum] != '\n'){
      pw_buf[hashNum] = currentLine[columnNum];
      hashNum++;
      columnNum++;
    }
    pw_buf[hashNum] = '\0';
    
    // Decode the encoded password hash
    BIO *enc_bio = BIO_new_mem_buf(pw_buf, SHA512_DIGEST_LENGTH_BASE64);
    BIO *b64_bio = BIO_new(BIO_f_base64());

    BIO_push(b64_bio, enc_bio);

    BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);

    BIO_read(b64_bio, passwords_out[*num_pass_out]->pass_hash, SHA512_DIGEST_LENGTH);

    BIO_free_all(b64_bio);
    
    *num_pass_out += 1;
  }

  fclose(passFile);

  return 0;
}


static int __pass_store_save(user_pass_t *passwords, size_t num_pass, int append)
{
  FILE *file;
  BIO *b64_bio = NULL;
  BIO *enc_bio = NULL;

  file = fopen(PASS_FILE_PATH, append ? "a+" : "w+");
  if (file) {

    // writes the username into the file
    fprintf(file, "%s", passwords->username);

    // writes the password hash type
    fprintf(file, ":$%d$", PASSWORD_HASH_TYPE);

    // writes the encoded password salt
    fprintf(file, "%s$", passwords->salt);

    // encodes the password hash into Base64
    // Base64 filter
    b64_bio = BIO_new(BIO_f_base64());
    // Memory buffer sink
    enc_bio = BIO_new(BIO_s_mem());

    // chain the Base64 filter to the memory buffer sink
    BIO_push(b64_bio, enc_bio);

    // Base64 encoding by default contains new lines.
    // Do not output new lines.
    BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);

    // Input data into the Base64 filter and flush the filter.
    BIO_write(b64_bio, passwords->pass_hash, SHA512_DIGEST_LENGTH);
    BIO_flush(b64_bio);

    // Get pointer and length of data in the memory buffer sink
    char *data_ptr = NULL;
    BIO_get_mem_data(enc_bio, &data_ptr);

    fprintf(file, "%s", data_ptr);
    
    if (data_ptr) free(data_ptr);

    fprintf(file, "\n");
    fclose(file);
  } else {
    if (b64_bio) BIO_free_all(b64_bio);
    return -1;
  }

  if (b64_bio) BIO_free_all(b64_bio);
  return 0;
}


/*
 * pass_store_add_user - adds a new user to the password store
 *
 * @username: NULL-delimited username string
 * @password: NULL-delimited password string
 *
 * Returns 0 on success, -1 on failure
 */
int pass_store_add_user(const char *username, const char *password)
{

  user_pass_t newUser;
  unsigned char salt[SALT_LEN];

  BIO *b64_bio;
  BIO *enc_bio;

  char *data_salt;
  long data_len;
  int pw_length;

  unsigned char *hashString;
  unsigned char hash[SHA512_DIGEST_LENGTH_BASE64];


  // Write the username to the struct
  strncpy(newUser.username, username, MAX_USER_LEN);
  
  // Create the password salt
  if (!RAND_bytes(salt, SALT_LEN)){
    return -1;
  }
  
  // Create the Base64 filter
  b64_bio = BIO_new(BIO_f_base64());
  
  // Create the memory buffer sink
  enc_bio = BIO_new(BIO_s_mem());
  
  // Chain the Base64 filter to the memory buffer sink
  BIO_push(b64_bio, enc_bio);
  
  // Base64 encoding by default contains new lines.
  // Do not output new lines.
  BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);
  
  // Input data into the Base64 filter and flush the filter.
  BIO_write(b64_bio, salt, SALT_LEN);
  BIO_flush(b64_bio);
  
  // Get pointer and length of data in the memory buffer sink
  data_salt = NULL;
  data_len = BIO_get_mem_data(enc_bio, &data_salt);
  
  if (data_len == -1) {
    BIO_free_all(b64_bio);
    return -1;
  } else {
    strncpy(newUser.salt, data_salt, SALT_LEN_BASE64+1);
  }
  
  // Generate the password string

  pw_length = strlen(password);

  hashString = (unsigned char *) malloc (1 + strlen(data_salt) + pw_length);
  strcpy((char *)hashString, password);
  hashString[pw_length - 1] = '\0';
  strcat((char *)hashString, data_salt);
  
  // Generate the password hash
  SHA512(hashString, sizeof(hashString) - 1, hash);
  
  // Write the unencoded password hash to struct
  strncpy((char* )newUser.pass_hash, (char *)hash, SHA512_DIGEST_LENGTH);
  if (hashString) free(hashString);

  // Write the new user data to the password store
  return __pass_store_save(&newUser, 1, 1);
}


/* 
 * pass_store_remove_user - removes a user from the password store
 *
 * @username: NULL-delimited username string
 *
 * Returns 0 on success, -1 on failure
 */
int pass_store_remove_user(const char *username)
{
  // count how many lines are in the password store file

  // fetch that many password structs from __pass_store_load
  user_pass_t **passwords_out = (user_pass_t**) malloc (50 * sizeof(user_pass_t*));
  int i, isFirst, wasFound;
  

  for (i = 0; i < 50; i++) {
    passwords_out[i] = (user_pass_t*) malloc (sizeof(user_pass_t));
  }
  size_t *num_pass_out = malloc(sizeof(size_t));
  __pass_store_load(passwords_out, num_pass_out);

  
  isFirst = 1;
  wasFound = -1;
  for (i = 0; i < *num_pass_out; i++) {
    // Check if the usernames match

    if (strcmp(passwords_out[i]->username, username) == 0) {
      wasFound = 0;
      continue;
    }

    __pass_store_save(passwords_out[i], 1, !isFirst);
    if (isFirst) isFirst = 0;
  }

  // Handle the edge case of only one password entry existing within the file
  if (isFirst && wasFound == 0) {
    FILE *file = fopen(PASS_FILE_PATH, "w");
    if (file) fclose(file);
  }


  // Free up memory
  free(num_pass_out);
  for (i = 0; i < 50; i++) {
    free(passwords_out[i]);
  }
  free(passwords_out);
  return wasFound;
}


/*
 * pass_store_check_password - check the password of a user
 *
 * @username: NULL-delimited username string
 * @passwrod: NULL-delimited password string
 *
 * Returns 0 on success, -1 on failure
 */
int pass_store_check_password(const char *username, const char *password)
{
  // count how many lines are in the password store file

  // fetch that many password structs from __pass_store_load
  user_pass_t **passwords_out = (user_pass_t**) malloc (50 * sizeof(user_pass_t*));

  char data_salt[SALT_LEN_BASE64+1];
  unsigned char *hashString;
  unsigned char hash[SHA512_DIGEST_LENGTH_BASE64];
  int i, h, isMatch;
  int pw_length;
  

  for (i = 0; i < 50; i++) {
    passwords_out[i] = (user_pass_t*) malloc (sizeof(user_pass_t));
  }
  size_t *num_pass_out = malloc(sizeof(size_t));
  __pass_store_load(passwords_out, num_pass_out);


  for (i = 0; i < *num_pass_out; i++) {
    // Check if the usernames match
    if (!strcmp(passwords_out[i]->username, username)) {

      strcpy(data_salt, passwords_out[i]->salt);

      pw_length = strlen(password);
      // Generate the password string
      hashString = (unsigned char *) malloc (1 + strlen(data_salt) + pw_length);

      strcpy((char *)hashString, password);
      hashString[pw_length - 1] = '\0';
      strcat((char *)hashString, data_salt);

      // Generate the password hash
      SHA512(hashString, sizeof(hashString) - 1, hash);

      //TODO SOMETHING NEEDS TO BE FIXED HERE
      isMatch = 1;
      for (h = 0; h < SHA512_DIGEST_LENGTH; h++) {
        if (hash[h] != passwords_out[i]->pass_hash[h]) {
          isMatch = 0;
          break;
        }
      }

      if (isMatch) {
        // Free up memory
        free(num_pass_out);
        for (i = 0; i < 50; i++) {
          free(passwords_out[i]);
        }
        free(passwords_out);
        free(hashString);
        return 0;
      }
      free(hashString);
    }
  }


  // Free up memory
  free(num_pass_out);
  for (i = 0; i < 50; i++) {
    free(passwords_out[i]);
  }
  free(passwords_out);
  return -1;
}

